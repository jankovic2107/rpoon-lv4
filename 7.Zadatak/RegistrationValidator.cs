﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class RegistrationValidator : IRegistrationValidator
    {
        EmailValidator EmailValidator = new EmailValidator();
        PasswordValidator passwordValidator = new PasswordValidator(7);

        public bool IsUserEntryValid(UserEntry entry)
        {
            if(EmailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password))
            {
                return true;
            }
            return false;
        }
    }
}
