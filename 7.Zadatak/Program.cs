﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            RegistrationValidator registrationValidator = new RegistrationValidator();
            Console.WriteLine(registrationValidator.IsUserEntryValid(UserEntry.ReadUserFromConsole()));
        }
    }
}
