﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(string candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return EndsWithComOrHr(candidate) && ContainsAtSymbol(candidate);
        }
        private bool EndsWithComOrHr(string candidate)
        {
            if (candidate.EndsWith(".com") || candidate.EndsWith(".hr"))
            {
                return true;
            }
            return false;
        }
        private bool ContainsAtSymbol(string candidate)
        {
            if (candidate.Contains('@'))
            {
                return true;
            }
            return false;
        }
    }
}
