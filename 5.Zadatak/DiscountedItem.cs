﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double discountBonus = 20;
        public DiscountedItem(IRentable rentable, double discountBonus) : base(rentable)
        {
            this.discountBonus = discountBonus;
        }
        public override double CalculatePrice()
        {
            return (base.CalculatePrice() - (base.CalculatePrice() * (this.discountBonus/100)));
        }
        public override String Description
        {
            get
            {
                return "Now at " + discountBonus + "% off: " + base.Description;
            }
        }
    }
}
