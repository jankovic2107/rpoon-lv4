﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rentables = new List<IRentable>();
            List<IRentable> flashSale = new List<IRentable>();
            RentingConsolePrinter Printer = new RentingConsolePrinter();
            Book book = new Book("Lord Of The Rings");
            Video movie = new Video("Shrek");
            HotItem hotBook = new HotItem(new Book("Game Of Thrones"));
            HotItem hotMovie = new HotItem(new Video("Godfather"));
            DiscountedItem discountedBook = new DiscountedItem(book, 20);
            DiscountedItem discountedMovie = new DiscountedItem(movie, 10);

            rentables.Add(book);
            rentables.Add(movie);

            rentables.Add(hotBook);
            rentables.Add(hotMovie);
            Printer.PrintTotalPrice(rentables);
            Printer.DisplayItems(rentables);

            Console.WriteLine();
            flashSale.Add(discountedBook);
            flashSale.Add(discountedMovie);
            Printer.PrintTotalPrice(flashSale);
            Printer.DisplayItems(flashSale);
        }
    }
}
