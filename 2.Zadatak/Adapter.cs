﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }

        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> data = dataset.GetData();
            int rowCount = data.Count();
            int columnCount = data[0].Count();
            double[][] matrix = new double[rowCount][];
            for(int i= 0; i < rowCount; i++)
            {
                matrix[i] = new double[columnCount];
            }

            for(int i = 0; i < rowCount; i++)
            {
                for(int j = 0; j< columnCount; j++)
                {
                    matrix[i][j] = data[i][j];
                }
            }
            return matrix;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
