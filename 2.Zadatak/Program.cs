﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset(@"C:\Users\Ivan Jankovic\Desktop\RPOON-LV4.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            foreach(var result in adapter.CalculateAveragePerColumn(dataset))
            {
                Console.Write(result + "\t");
            }
            Console.WriteLine();
            foreach (var result in adapter.CalculateAveragePerRow(dataset))
            {
                Console.Write(result + "\t");
            }
        }
    }
}
