﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailValidator email = new EmailValidator();
            PasswordValidator password = new PasswordValidator(7);

            Console.WriteLine(email.IsValidAddress("jankovic@gmail.com"));
            Console.WriteLine(password.IsValidPassword("Danas2jetaj1dan"));

        }
    }
}
