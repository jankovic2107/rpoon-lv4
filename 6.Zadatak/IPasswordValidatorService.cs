﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
