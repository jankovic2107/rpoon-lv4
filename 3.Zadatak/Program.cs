﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter Printer = new RentingConsolePrinter();
            Book book = new Book("Lord Of The Rings");
            Video video = new Video("Shrek");
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(book);
            rentables.Add(video);
            Printer.PrintTotalPrice(rentables);
            Printer.DisplayItems(rentables);

        }
    }
}
