﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter Printer = new RentingConsolePrinter();
            Book book = new Book("Lord Of The Rings");
            Video movie = new Video("Shrek");
            HotItem hotBook = new HotItem(new Book("Game Of Thrones"));
            HotItem hotMovie = new HotItem(new Video("Godfather"));
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(book);
            rentables.Add(movie);
            rentables.Add(hotBook);
            rentables.Add(hotMovie);
            Printer.PrintTotalPrice(rentables);
            Printer.DisplayItems(rentables);

        }
    }
}
